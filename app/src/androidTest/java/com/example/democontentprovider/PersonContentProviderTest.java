package com.example.democontentprovider;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import androidx.room.Room;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.example.democontentprovider.Constants.*;
import static com.example.democontentprovider.provider.PersonContentProvider.AUTHORITY;
import static com.example.democontentprovider.provider.PersonContentProvider.PERSON_TABLE_NAME;
import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class PersonContentProviderTest {

    private static final String TAG = PersonContentProviderTest.class.getName();

    private static final Uri PERSON_URI = Uri.parse("content://" + AUTHORITY + "/" + PERSON_TABLE_NAME);
    private static final Uri PERSON_URI_ID = Uri.parse("content://" + AUTHORITY + "/" + PERSON_TABLE_NAME + "/" + "1");

    private ContentResolver contentResolver;

    @Before
    public void setUp() {
        Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getInstrumentation().getContext(),
                ApplicationDatabase.class).allowMainThreadQueries().build();
        contentResolver = InstrumentationRegistry.getInstrumentation().getContext().getContentResolver();
    }

    @Test
    public void insertPersonData() {
        Uri insertUri = contentResolver.insert(PERSON_URI, getContentValues());
        assertNotNull(insertUri);
    }

    @Test
    public void queryPerson() {
        Cursor cursor = contentResolver.query(PERSON_URI, null, null,
                null, null);
        assertNotNull(cursor);

        if (cursor.moveToFirst()) {
            do {
                String name = cursor.getString(cursor.getColumnIndex(PERSON_NAME));
                String gender = cursor.getString(cursor.getColumnIndex(PERSON_GENDER));
                String emailaddress = cursor.getString(cursor.getColumnIndex(PERSON_EMAIL_ADDRESS));
                Log.d(TAG, "queryPerson: " + name + " " + gender + " " + emailaddress);
            }
            while (cursor.moveToNext());
        }
        cursor.close();

    }

    @Test
    public void personDelete() {
        Uri itemUri = contentResolver.insert(PERSON_URI, getContentValues());
        assertNotNull(itemUri);

        Cursor cursor = contentResolver.query(PERSON_URI, new String[]{PERSON_NAME}, null,
                null, null);
        assertNotNull(cursor);
        cursor.close();

        int count = contentResolver.delete(itemUri, null, null);
        assertNotNull(count);

    }

    @Test
    public void delete() {
        int count = contentResolver.delete(PERSON_URI_ID, null, null);
        assertNotNull(count);
    }

    private ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(PERSON_NAME, "Rashmi");
        contentValues.put(PERSON_GENDER, "female");
        contentValues.put(PERSON_EMAIL_ADDRESS, "123@gmail.com");
        return contentValues;
    }


}
