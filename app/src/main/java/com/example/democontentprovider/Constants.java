package com.example.democontentprovider;

public class Constants {

    public static final String PERSON_ID = "id";
    public static final String PERSON_NAME = "name";
    public static final String PERSON_GENDER = "gender";
    public static final String PERSON_EMAIL_ADDRESS = "emailAddress";
}
