package com.example.democontentprovider.model;

import android.content.ContentValues;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import static com.example.democontentprovider.Constants.*;

@Entity(tableName = "person_info")
public class Person {

    @PrimaryKey(autoGenerate = true)
    private Integer id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "gender")
    private String gender;

    @ColumnInfo(name = "emailAddress")
    private String emailAddress;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public static Person fromContentValues(ContentValues contentValues) {
        Person person = new Person();
        if (contentValues.containsKey(PERSON_ID)) {
            person.setId(contentValues.getAsInteger(PERSON_ID));
        } if (contentValues.containsKey(PERSON_NAME)) {
            person.setName(contentValues.getAsString(PERSON_NAME));
        } if (contentValues.containsKey(PERSON_GENDER)) {
            person.setGender(contentValues.getAsString(PERSON_GENDER));
        } if (contentValues.containsKey(PERSON_EMAIL_ADDRESS)) {
            person.setEmailAddress(contentValues.getAsString(PERSON_EMAIL_ADDRESS));
        }
        return person;
    }
}
