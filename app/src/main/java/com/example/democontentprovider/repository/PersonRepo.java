package com.example.democontentprovider.repository;

import android.database.Cursor;

import com.example.democontentprovider.ApplicationDatabase;
import com.example.democontentprovider.dao.PersonDao;
import com.example.democontentprovider.model.Person;

public class PersonRepo {

    public static final String TAG = PersonRepo.class.getName();

    private PersonDao personDao;
    private static PersonRepo INSTANCE;

    private PersonRepo(ApplicationDatabase applicationDatabase) {
        personDao = applicationDatabase.getPersonDao();
    }

    public static PersonRepo getInstance(ApplicationDatabase applicationDatabase) {
        if (INSTANCE == null) {
            INSTANCE = new PersonRepo(applicationDatabase);
        }
        return INSTANCE;
    }

    public long insert(Person media) {
        return personDao.insert(media);
    }

    public Cursor findAll() {
        return personDao.findAll();
    }
}
