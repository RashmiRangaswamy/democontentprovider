package com.example.democontentprovider;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.democontentprovider.dao.PersonDao;
import com.example.democontentprovider.model.Person;

@Database(entities = {Person.class}, version = 2)
public abstract class ApplicationDatabase extends RoomDatabase {

    private static final String DATABASE_NAME = "testdb";
    private static ApplicationDatabase INSTANCE;

    public static ApplicationDatabase getInstance(Context context){
        if (INSTANCE == null) {
            INSTANCE = Room
                    .databaseBuilder(context, ApplicationDatabase.class, DATABASE_NAME)
                    .allowMainThreadQueries().fallbackToDestructiveMigration().build();
        }
        return INSTANCE;
    }

    public abstract PersonDao getPersonDao();

}
