package com.example.democontentprovider;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getFiles();
    }

    private void getFiles() {
        File file = new File("/data/data/com.example.democontentprovider");
        File[] files = file.listFiles();
        for (File f:
             files) {
            Log.d("Main Activity", f.getName());
        }
    }
}
