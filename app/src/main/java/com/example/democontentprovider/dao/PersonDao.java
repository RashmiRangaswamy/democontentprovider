package com.example.democontentprovider.dao;

import android.database.Cursor;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.democontentprovider.model.Person;

@Dao
public interface PersonDao {

    /**
     * @param person Insert a person data into the table
     * @return row ID fo newly inserted data
     */
    @Insert
    long insert(Person person);

    /**
     * select all person
     * @return A {@link Cursor} of all person in the table
     */
    @Query("SELECT * FROM person_info")
    Cursor findAll();

    /**
     * Delete a person by ID
     * @return A number of persons deleted
     */
    @Query("DELETE FROM person_info WHERE id = :id ")
    int delete(long id);

    /**
     * Update the person
     * @return A number of persons updated
     */
    @Update
    int update(Person person);
}
